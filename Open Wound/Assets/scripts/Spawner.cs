﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public GameObject[] cells;
	int cellsNo;
	public float maxPos = 2.1f;
	public float delayTimer = 1f;

	float timer;

	// Use this for initialization
	void Start () {
		timer = delayTimer;

	}
	
	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime;
		if (timer <= 0) {
			Vector3 cellPos = new Vector3 (Random.Range (-2.1f, 2.1f), transform.position.y, transform.position.z);
			cellsNo = Random.Range (0, 5);
			Instantiate (cells[cellsNo], cellPos, transform.rotation);
			timer = delayTimer;
		}
			
	}
}
