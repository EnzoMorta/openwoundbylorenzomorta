﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Player") {
			Destroy (gameObject);
		}
		if (col.gameObject.tag == "Finish") {
			Destroy (gameObject);
		}
		if (col.gameObject.tag == "Enemy") {
			Destroy (gameObject);
		}
	}
}
